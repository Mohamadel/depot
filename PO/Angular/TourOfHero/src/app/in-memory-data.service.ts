import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 1, name: 'Mouhamad SwS' },
      { id: 2, name: 'Seydina Abou Bakr RA' },
      { id: 3, name: 'Seydina Oumar RA' },
      { id: 4, name: 'Seydina Ousmane RA' },
      { id: 5, name: 'Seydina Aliou RA' },
      { id: 6, name: 'Cheikh Tidiane RA' },
      { id: 7, name: 'ElHadji Oumar RA' },
      { id: 8, name: 'ElHadji Malick RA' },
      { id: 9, name: 'Nasrou Dine BA RA' },
      { id: 10, name: 'Thierno Madiakhaté RA' }
      ];

    return {heroes};
  }

  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}
