import { Hero } from './hero';
//Liste de heros
export const HEROES: Hero[] = [
  { id: 1, name: 'Mouhamad SwS' },
  { id: 2, name: 'Seydina Abou Bakr RA' },
  { id: 3, name: 'Seydina Oumar RA' },
  { id: 4, name: 'Seydina Ousmane RA' },
  { id: 5, name: 'Seydina Aliou RA' },
  { id: 6, name: 'Cheikh Tidiane RA' },
  { id: 7, name: 'ElHadji Oumar RA' },
  { id: 8, name: 'ElHadji Malick RA' },
  { id: 9, name: 'AlHadji Mamadou Seydou RA' },
  { id: 10, name: 'Thierno Madiakhaté RA' }
];
