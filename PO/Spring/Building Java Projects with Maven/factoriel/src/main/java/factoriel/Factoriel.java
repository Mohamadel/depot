package factoriel;
/*claase permettant de calculer
 le factoriel d'un entier*/
public class Factoriel{
	private double fact = 1;
	private int i;
	
	/*Methode permettant de determiner 
	le factoriel d'un entier*/
	public double facto(int n){
		
		/*boucle permettant 
		de determiner le factoriel*/
		for(i = 1; i<=n; i++){
			fact*=i;
		}
		//retourne le factoriel de l'entier
		return fact;
	}
}