package factoriel;

import java.util.Scanner;

/*Classe permettant d'afficher 
le factoriel de l'entier saisi*/
public class AffichageFactoriel{
	
	public static void main(String[] arg){
		/*Création d'un abjet qui sera utilisé 
		pour afficher le resultat*/
		Factoriel resultat = new Factoriel();
		
		/*Creation d'un objet permettant 
		de saisir une valeur*/
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Veuillez saisir un entier");
		//Déclaration permettant de saisir un entier
		int n = sc.nextInt();
		//Affiche le résultat
		System.out.println(""+n+"! ="+resultat.facto(n));
	}
}