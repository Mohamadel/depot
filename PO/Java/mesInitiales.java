import java.util.Sacanner;
/*Classe permettant d'afficher les initials 
à partir de prenom et nom saisi*/
public class afficherInitial{
	public static void main(String[] args){
		int i;
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		//Définition d'un objet conteneur de string
		StringBuffer mesInitiaux = new StringBuffer();
		
		System.out.println("Veuilez siasir votre prenom et nom");
		String prenomNom = sc.nextLine();
		
		int taillePrenomNom = prenomNom.length();
		/*La boucle vérifie les caratère en majuscule
		contenue dans la chaine de caratère saisi*/
		for(i = 0; i<=taillePrenomNom; i++){
			/*la "if" à partir de "isUppercase" 
			vérifie les caratère majuscule*/
			if(Character.isUpperCase(prenomNom.charAt(i))){
				//stocke les initiaux dans "mesInitiaux"
				//La methode append() converti les donnée en chaine de caractère
				mesInitiaux.append(prenomNom.charAt(i));
			}
		}
		//Permet d'afficher le resultas, c'est-à-dire les initiaux
		System.out.println("Voici tes initial "+mesInitiaux);
	}	
}