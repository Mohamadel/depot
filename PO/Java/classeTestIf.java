//Classe permettant de tester la structure de condition if...else
class TestIf{
	public static void main (String[] args){
		
		int aNumber = 3;
		
		/*Si aNumber est superieur ou egale à 0
		les instructions dans le accolade sont executer*/
		if (aNumber> = 0){
			/*Si aNumber est egale à 0 
			le programme affiche première chaîne*/
			if (aNumber == 0){
				 System.out.println ("première chaîne");	
			}
			/*Si aNumber est différent de 0
			le programme affiche deuxième chaîne*/	
			else{
				System.out.println ("deuxième chaîne");
				}
		}
		/*Si aNumber est negatif
		le programme affiche troisième*/
		System.out.println ("troisième chaîne");
	}
}