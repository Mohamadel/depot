//Definition d'une annotation
public @interface DemandeAmeliaration {
	int id ();
	String synopsis();
	String ingenieur() default "unassigned";
	String date() default "unknow";
}