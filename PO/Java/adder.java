import java.util.Scanner;

//Classe qui additionne des entiers saisis au clavier
public class addition{
public static void main(String[] args){
		
		int n;
		int m = 0;
		int i; 
		int j;
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		
		/*Affiche un message qui demande 
		à l'utilisateur de saisir 
		le nombre d'entier à additionner*/
		System.out.println("Veuillez saisir le nombre d'entier que vous voulez additionner");
		j = sc.nextInt();
		
		//L'opération ne sera exécuter que si j est supérier ou égale à 2
		if(j<2){
			System.out.println("La valeur est inférieur à 2");
		}
		else {
			/*La boucle s'arrête si le nombre d'entier saisi 
			est égale aux nombre d'entiers demandé c'est-à-dire i = j*/
			for(i = 0; i<=j; i++){
				
				System.out.println("Veuillez saisir un entier");
				n = sc.nextInt();
				
				//Cette ligne fait la somme des entiers saisis
				m += n;
			}
			//Affiche la somme des entiers saisis
			System.out.println("La somme est de : "+m);
		}
		
	}
}