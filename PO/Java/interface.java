//Interface pour une definition du temps local
public interface TempsLocal{
	int heureLocal(int heure, int minute, int seconde);
	int dateLocal(int jour, int mois, int annee);
}