/*Classe permettant d'initialise 
et d'afficher des nombre réel*/
public class NumberHolder{
	
	//variable de type entier
	public int aInt;
	//variable de type flotteur
	public float aFloat;
	
	//Constructeur d'initialisation
	public NumberHolder(){
		this.aInt = 12;
		this.aFloat = (float) 12.12;
	}
	//retourne l'entier aInt
	public int getaInt(){
		return aInt;
	}
	//retourne le nombre flotteur aFloat
	public float getaFloat(){
		return aFloat;
	}
	
	public static void main(String[] args){
		//instaciation de la classe NumberHolder
		NumberHolder nombre = new NumberHolder();
		
		//affiche l'entier
		System.out.println("L'entier est "+nombre.getaInt());
		//affiche le nombre flotteur
		System.out.println("Le flotteur est "+nombre.getaFloat());
	}
}